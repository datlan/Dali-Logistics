export default function ({ $axios }) {
    $axios.setHeader('Content-Type', 'application/x-www-form-urlencoded', [
        'post'
    ])
}