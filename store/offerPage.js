import Cookie from 'js-cookie'

export const state = () => ({
    offerPageData: {},
})
export const getters = {
    getOfferPageData(state) { return state.offerPageData },
}
export const mutations = {
    SET_OFFER_PAGE_DATA(state, data) {
        state.offerPageData = data;
    },
}
export const actions = {
    async getOfferPageData({ commit }, payload) {
        try {
            const lang = Cookie.get('lang')
            this.$axios.get(`/services?page=${payload}`, {
                headers: {
                    "Content-Type": "application/json",
                    "X-Localization": lang
                }
            })
                .then((result) => {
                    commit('SET_OFFER_PAGE_DATA', result.data);
                })
                .catch((err) => {
                    console.log(err);
                });

        } catch (error) {
            console.log(error);
        }
    },
}