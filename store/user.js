import Cookie from 'js-cookie'
export const state = () => ({
    profile: [
        {
            id: 0,
            name: 'user',
            menu: [
                {
                    id: 0,
                    title_en: 'Home',
                    title_ru: 'Главный',
                    path: '',
                    icon: 'admin-account',
                    status: true
                },
                {
                    id: 1,
                    title_en: 'My applications',
                    title_ru: 'Мои заявки',
                    path: 'applications',
                    icon: 'admin-application',
                    status: false,
                },
                {
                    id: 2,
                    title_en: 'My addresses',
                    title_ru: 'Мои адреса',
                    path: 'addresses',
                    icon: 'admin-brokers',
                    status: false,
                },
                {
                    id: 3,
                    title_en: 'Directory',
                    title_ru: 'Справочник',
                    path: 'directory',
                    icon: 'admin-content-manager',
                    status: false,
                },
                {
                    id: 4,
                    title_en: 'Chat',
                    title_ru: 'Чат',
                    path: 'chat-operator',
                    icon: "admin-online-chat",
                    status: false,
                },
                {
                    id: 5,
                    title_en: 'Settings',
                    title_ru: 'Настройки',
                    path: 'settings',
                    icon: 'admin-content-manager',
                    status: false,
                },
            ],
            addresses: [
                {
                    id: 0,
                    country: 'Uzbekistan',
                    region: 'Namangan',
                    city: 'Chortoq',
                    street: 'kokcha, 84',
                    postcode: '161100',
                },
            ]
        },
        {
            id: 1,
            name: 'logist',
            menu: [
                {
                    id: 0,
                    title: 'General applications',
                    path: 'general-applications',
                    icon: 'admin-account',
                    status: true,
                },
                {
                    id: 1,
                    title: 'My applications',
                    path: 'my-applications',
                    icon: 'admin-application',
                    status: false,
                },
                {
                    id: 2,
                    title: 'Directory',
                    path: 'directory',
                    icon: 'admin-content-manager',
                    status: false,
                },
                {
                    id: 3,
                    title: 'Chat',
                    path: 'chat-operator',
                    icon: "admin-online-chat",
                    status: false,
                },
                {
                    id: 4,
                    title: 'Templates',
                    path: 'templates',
                    icon: 'admin-application',
                    status: false,
                }
            ]
        },
        {
            id: 2,
            name: 'operator',
            menu: [
                {
                    id: 0,
                    title: 'General applications',
                    path: 'general-applications',
                    icon: 'admin-account',
                    status: true,
                },
                {
                    id: 1,
                    title: 'My applications',
                    path: 'my-applications',
                    icon: 'admin-application',
                    status: false,
                },
                {
                    id: 2,
                    title: 'Directory',
                    path: 'directory',
                    icon: 'admin-content-manager',
                    status: false,
                },
                {
                    id: 3,
                    title: 'Chat',
                    path: 'chat-operator',
                    icon: "admin-online-chat",
                    status: false,
                },
                {
                    id: 4,
                    title: 'Templates',
                    path: 'templates',
                    icon: 'admin-application',
                    status: false,
                }
            ]
        }
    ],
    directory: [],
    operatorDirectory: [],
    logisDirectory: [],
    applications: [],
    applicationById: [],
    operatorApplications: [],
    logisApplications: [],
    operator: [],
    logis: [],
    operatorById: [],
    logisById: [],
    addresses: []
})

export const getters = {
    getProfile(state) { return state.profile },
    getOperator(state) { return state.operator },
    getLogis(state) { return state.logis },
    getDirectory(state) { return state.directory },
    getOperatorDirectory(state) { return state.operatorDirectory },
    getLogisDirectory(state) { return state.logisDirectory },
    getApplication(state) { return state.applications },
    getOperatorApplication(state) { return state.operatorApplications },
    getLogisApplication(state) { return state.logisApplications },
    getApplicationById(state) { return state.applicationById },
    getOperatorById(state) { return state.operatorById },
    getLogisById(state) { return state.logisById },
    getAddresses(state) { return state.addresses }
}

export const mutations = {
    SET_PROFILE(state, name, id) {
        state.profile.forEach(element => {
            if (element.name == name) {
                element.menu.forEach(elem => {
                    elem.status = false;
                })
            }
        });
        state.profile.forEach(element => {
            if (element.name == name) {
                element.menu.forEach(elem => {
                    if (elem.id == id) {
                        elem.status = true;
                    }
                })
            }
        });
    },
    SET_OPERATOR(state, payload) {
        state.operator = payload
    },
    SET_LOGIS(state, payload) {
        state.logis = payload
    },
    SET_DIRECTORY(state, payload) {
        state.directory = payload
    },
    SET_OPERATOR_DIRECTORY(state, payload) {
        state.operatorDirectory = payload
    },
    SET_LOGIS_DIRECTORY(state, payload) {
        state.logisDirectory = payload
    },
    SET_APPLICATION(state, payload) {
        state.applications = payload
    },
    SET_OPERATOR_APPLICATION(state, payload) {
        state.operatorApplications = payload
    },
    SET_LOGIS_APPLICATION(state, payload) {
        state.logisApplications = payload
    },
    SET_APPLICATION_BY_ID(state, payload) {
        state.applicationById = payload
    },
    SET_OPERATOR_BY_ID(state, payload) {
        state.operatorById = payload
    },
    SET_LOGIS_BY_ID(state, payload) {
        state.logisById = payload
    },
    SET_ADDRESSES(state, payload) {
        state.addresses = payload
    }
}

export const actions = {
    async getDirectory({ commit }) {
        try {
            if (Cookie.get("token")) {
                const token = Cookie.get("token")
                const lang = Cookie.get('lang')
                this.$axios.get(`/user/directory`, {
                    headers: {
                        "Content-Type": "application/json",
                        "X-Localization": lang,
                        Authorization: `Bearer ${token}`,
                    }
                })
                    .then((result) => {
                        commit('SET_DIRECTORY', result.data);
                    })
                    .catch((err) => {
                        console.log(err);
                    });
            }

        } catch (error) {
            console.log(error);
        }
    },

    async getOperatorDirectory({ commit }) {
        try {
            if (Cookie.get("token")) {
                const token = Cookie.get("token")
                const lang = Cookie.get('lang')
                this.$axios.get(`/operator/directory`, {
                    headers: {
                        "Content-Type": "application/json",
                        "X-Localization": lang,
                        Authorization: `Bearer ${token}`,
                    }
                })
                    .then((result) => {
                        commit('SET_OPERATOR_DIRECTORY', result.data);
                    })
                    .catch((err) => {
                        console.log(err);
                    });
            }

        } catch (error) {
            console.log(error);
        }
    },

    async getLogisDirectory({ commit }) {
        try {
            if (Cookie.get("token")) {
                const token = Cookie.get("token")
                const lang = Cookie.get('lang')
                this.$axios.get(`/logis/directory`, {
                    headers: {
                        "Content-Type": "application/json",
                        "X-Localization": lang,
                        Authorization: `Bearer ${token}`,
                    }
                })
                    .then((result) => {
                        commit('SET_LOGIS_DIRECTORY', result.data);
                    })
                    .catch((err) => {
                        console.log(err);
                    });
            }

        } catch (error) {
            console.log(error);
        }
    },

    async getApplication({ commit }, _payload, page) {
        try {
            if (Cookie.get("token")) {
                let token = Cookie.get("token")
                const lang = Cookie.get('lang')
                if (_payload) {
                    this.$axios.get(`/user/application?page=${page}`, {
                        params: _payload,
                        headers: {
                            "Content-Type": "application/json",
                            "X-Localization": lang,
                            Authorization: `Bearer ${token}`,
                        }
                    })
                        .then((result) => {
                            commit('SET_APPLICATION', result.data.data);
                        })
                        .catch((err) => {
                            console.log(err);
                        });
                } else {
                    this.$axios.get(`/user/application=${page}`, {
                        headers: {
                            "Content-Type": "application/json",
                            "X-Localization": lang,
                            Authorization: `Bearer ${token}`,
                        }
                    })
                        .then((result) => {
                            commit('SET_APPLICATION', result.data.data);
                        })
                        .catch((err) => {
                            console.log(err);
                        });

                }
            }

        } catch (error) {
            console.log(error);
        }
    },

    async getOperatorApplication({ commit }) {
        try {
            if (Cookie.get("token")) {
                let token = Cookie.get("token")
                const lang = Cookie.get('lang')
                this.$axios.get(`/operator/home`, {
                    headers: {
                        "Content-Type": "application/json",
                        "X-Localization": lang,
                        Authorization: `Bearer ${token}`,
                    }
                })
                    .then((result) => {
                        commit('SET_OPERATOR_APPLICATION', result.data.data);
                    })
                    .catch((err) => {
                        console.log(err);
                    });
            }

        } catch (error) {
            console.log(error);
        }
    },

    async getLogisApplication({ commit }) {
        try {
            if (Cookie.get("token")) {
                let token = Cookie.get("token")
                const lang = Cookie.get('lang')
                this.$axios.get(`/logis/home`, {
                    headers: {
                        "Content-Type": "application/json",
                        "X-Localization": lang,
                        Authorization: `Bearer ${token}`,
                    }
                })
                    .then((result) => {
                        commit('SET_LOGIS_APPLICATION', result.data.data);
                    })
                    .catch((err) => {
                        console.log(err);
                    });
            }

        } catch (error) {
            console.log(error);
        }
    },

    async getOperator({ commit }, payload) {
        try {
            if (Cookie.get("token")) {
                let token = Cookie.get("token")
                const lang = Cookie.get('lang')
                if (payload) {
                    this.$axios.get(`/operator/application`, {
                        params: payload,
                        headers: {
                            "Content-Type": "application/json",
                            "X-Localization": lang,
                            Authorization: `Bearer ${token}`,
                        }
                    })
                        .then((result) => {
                            commit('SET_OPERATOR', result.data.data.data);
                        })
                        .catch((err) => {
                            console.log(err);
                        });
                } else {
                    this.$axios.get(`/operator/application`, {
                        headers: {
                            "Content-Type": "application/json",
                            "X-Localization": lang,
                            Authorization: `Bearer ${token}`,
                        }
                    })
                        .then((result) => {
                            commit('SET_OPERATOR', result.data.data.data);
                        })
                        .catch((err) => {
                            console.log(err);
                        });

                }
            }

        } catch (error) {
            console.log(error);
        }
    },

    async getLogis({ commit }, payload) {
        try {
            if (Cookie.get("token")) {
                let token = Cookie.get("token")
                const lang = Cookie.get('lang')
                if (payload) {
                    this.$axios.get(`/logis/application`, {
                        params: payload,
                        headers: {
                            "Content-Type": "application/json",
                            "X-Localization": lang,
                            Authorization: `Bearer ${token}`,
                        }
                    })
                        .then((result) => {
                            commit('SET_LOGIS', result.data.data.data);
                        })
                        .catch((err) => {
                            console.log(err);
                        });
                } else {
                    this.$axios.get(`/logis/application`, {
                        headers: {
                            "Content-Type": "application/json",
                            "X-Localization": lang,
                            Authorization: `Bearer ${token}`,
                        }
                    })
                        .then((result) => {
                            commit('SET_LOGIS', result.data.data.data);
                        })
                        .catch((err) => {
                            console.log(err);
                        });

                }
            }

        } catch (error) {
            console.log(error);
        }
    },

    async getApplicationById({ commit }, payload) {
        try {
            if (Cookie.get("token")) {
                let token = Cookie.get("token")
                const lang = Cookie.get('lang')
                this.$axios.get(`/user/application/${payload}`, {
                    headers: {
                        "Content-Type": "application/json",
                        "X-Localization": lang,
                        Authorization: `Bearer ${token}`,
                    }
                })
                    .then((result) => {
                        commit('SET_APPLICATION_BY_ID', result.data.data);
                    })
                    .catch((err) => {
                        console.log(err);
                    });
            }

        } catch (error) {
            console.log(error);
        }
    },

    async getOperatorById({ commit }, payload) {
        try {
            if (Cookie.get("token")) {
                let token = Cookie.get("token")
                const lang = Cookie.get('lang')
                this.$axios.get(`/operator/application/${payload}`, {
                    headers: {
                        "Content-Type": "application/json",
                        "X-Localization": lang,
                        Authorization: `Bearer ${token}`,
                    }
                })
                    .then((result) => {
                        commit('SET_OPERATOR_BY_ID', result.data.data);
                    })
                    .catch((err) => {
                        console.log(err);
                    });
            }

        } catch (error) {
            console.log(error);
        }
    },

    async getLogisById({ commit }, payload) {
        try {
            if (Cookie.get("token")) {
                let token = Cookie.get("token")
                const lang = Cookie.get('lang')
                this.$axios.get(`/logis/application/${payload}`, {
                    headers: {
                        "Content-Type": "application/json",
                        "X-Localization": lang,
                        Authorization: `Bearer ${token}`,
                    }
                })
                    .then((result) => {
                        commit('SET_LOGIS_BY_ID', result.data.data);
                    })
                    .catch((err) => {
                        console.log(err);
                    });
            }

        } catch (error) {
            console.log(error);
        }
    },

    async getAddresses({ commit }) {
        try {
            if (Cookie.get("token")) {
                let token = Cookie.get("token")
                const lang = Cookie.get('lang')
                this.$axios.get(`/user/address/index`, {
                    headers: {
                        "Content-Type": "application/json",
                        "X-Localization": lang,
                        Authorization: `Bearer ${token}`,
                    }
                })
                    .then((result) => {
                        commit('SET_ADDRESSES', result.data.data);
                    })
                    .catch((err) => {
                        console.log(err);
                    });
            }

        } catch (error) {
            console.log(error);
        }
    }
}