import Cookie from 'js-cookie'
export const state = () => ({
    countryList: {},
    senderCitiesList: {},
    senderRegionsList: {},
    receiverCitiesList: {},
    receiverRegionsList: {},
    conditionList: {},
    methodList: {},
    colliList: {},
    isNewApplication: false,
    changingApplication: {},
})
export const getters = {
    getCountryList(state) { return state.countryList },
    getSenderCitiesList(state) { return state.senderCitiesList },
    getSenderRegionsList(state) { return state.senderRegionsList },
    getConditionList(state) { return state.conditionList },
    getReceiverCitiesList(state) { return state.receiverCitiesList },
    getReceiverRegionsList(state) { return state.receiverRegionsList },
    getMethodList(state) { return state.methodList },
    getColliList(state) { return state.colliList },
    getIsNewApplication(state) { return state.isNewApplication },
    getChangingApplication(state) { return state.changingApplication }
}
export const mutations = {
    SET_COUNTRY_LIST(state, data) {
        state.countryList = data;
    },
    SET_SENDER_CITIES_LIST(state, data) {
        state.senderCitiesList = data;
    },
    SET_SENDER_REGIONS_LIST(state, data) {
        state.senderRegionsList = data;
    },
    SET_CONDITION_LIST(state, data) {
        state.conditionList = data;
    },
    SET_RECEIVER_CITIES_LIST(state, data) {
        state.receiverCitiesList = data;
    },
    SET_RECEIVER_REGIONS_LIST(state, data) {
        state.receiverRegionsList = data;
    },
    SET_METHOD_LIST(state, data) {
        state.methodList = data;
    },
    SET_COLLI_LIST(state, data) {
        state.colliList = data;
    },
    SET_IS_NEW_APPLICATION(state, data) {
        state.isNewApplication = data;
    },
    SET_CHANGING_APPLICATION(state, data) {
        state.changingApplication = data;
    }
}
export const actions = {
    async getCountryList({ commit }) {
        try {
            const lang = Cookie.get('lang')
            this.$axios.get("/dictionary/country", {
                headers: {
                    "Content-Type": "application/json",
                    "X-Localization": lang,
                }
            })
                .then((result) => {
                    commit('SET_COUNTRY_LIST', result.data);
                })
                .catch((err) => {
                    console.log(err);
                });

        } catch (error) {
            console.log(error);
        }
    },
    async getSenderCitiesList({ commit }, payload) {
        try {
            const lang = Cookie.get('lang')
            this.$axios.get(`/dictionary/city/${payload}`, {
                headers: {
                    "Content-Type": "application/json",
                    "X-Localization": lang,
                }
            })
                .then((result) => {
                    commit('SET_SENDER_CITIES_LIST', result.data);
                })
                .catch((err) => {
                    console.log(err);
                });

        } catch (error) {
            console.log(error);
        }
    },
    async getSenderRegionsList({ commit }, payload) {
        try {
            const lang = Cookie.get('lang')
            this.$axios.get(`/dictionary/region/${payload}`, {
                headers: {
                    "Content-Type": "application/json",
                    "X-Localization": lang,
                }
            })
                .then((result) => {
                    commit('SET_SENDER_REGIONS_LIST', result.data);
                })
                .catch((err) => {
                    console.log(err);
                });

        } catch (error) {
            console.log(error);
        }
    },
    async getConditionList({ commit }) {
        try {
            const lang = Cookie.get('lang')
            this.$axios.get(`/dictionary/condition`, {
                headers: {
                    "Content-Type": "application/json",
                    "X-Localization": lang,
                }
            })
                .then((result) => {
                    commit('SET_CONDITION_LIST', result.data);
                })
                .catch((err) => {
                    console.log(err);
                });

        } catch (error) {
            console.log(error);
        }
    },
    async getReceiverCitiesList({ commit }, payload) {
        try {
            const lang = Cookie.get('lang')
            this.$axios.get(`/dictionary/city/${payload}`, {
                headers: {
                    "Content-Type": "application/json",
                    "X-Localization": lang,
                }
            })
                .then((result) => {
                    commit('SET_RECEIVER_CITIES_LIST', result.data);
                })
                .catch((err) => {
                    console.log(err);
                });

        } catch (error) {
            console.log(error);
        }
    },
    async getReceiverRegionsList({ commit }, payload) {
        try {
            const lang = Cookie.get('lang')
            this.$axios.get(`/dictionary/region/${payload}`, {
                headers: {
                    "Content-Type": "application/json",
                    "X-Localization": lang,
                }
            })
                .then((result) => {
                    commit('SET_RECEIVER_REGIONS_LIST', result.data);
                })
                .catch((err) => {
                    console.log(err);
                });

        } catch (error) {
            console.log(error);
        }
    },
    async getMethodList({ commit }) {
        try {
            const lang = Cookie.get('lang')
            this.$axios.get(`/dictionary/delivery`, {
                headers: {
                    "Content-Type": "application/json",
                    "X-Localization": lang,
                }
            })
                .then((result) => {
                    commit('SET_METHOD_LIST', result.data);
                })
                .catch((err) => {
                    console.log(err);
                });

        } catch (error) {
            console.log(error);
        }
    },
    async getColliList({ commit }) {
        try {
            const lang = Cookie.get('lang')
            this.$axios.get(`/dictionary/colli`, {
                headers: {
                    "Content-Type": "application/json",
                    "X-Localization": lang,
                }
            })
                .then((result) => {
                    commit('SET_COLLI_LIST', result.data);
                })
                .catch((err) => {
                    console.log(err);
                });

        } catch (error) {
            console.log(error);
        }
    },

}