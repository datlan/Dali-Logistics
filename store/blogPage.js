import Cookie from 'js-cookie'

export const state = () => ({
    blogPageData: {},
})
export const getters = {
    getBlogPageData(state) { return state.blogPageData },
}
export const mutations = {
    SET_BLOG_PAGE_DATA(state, data) {
        state.blogPageData = data;
    },
}
export const actions = {
    async getBlogPageData({ commit }, payload) {
        try {
            const lang = Cookie.get('lang')
            this.$axios.get(`/news?page=${payload}`, {
                headers: {
                    "Content-Type": "application/json",
                    "X-Localization": lang
                }
            })
                .then((result) => {
                    commit('SET_BLOG_PAGE_DATA', result.data);
                })
                .catch((err) => {
                    console.log(err);
                });

        } catch (error) {
            console.log(error);
        }
    },
}