export const state = () => ({
    typeApplication: {}
})
export const getters = {
    getTypeApplication() { return localStorage.getItem('type') },
}
export const mutations = {
    SET_TYPE_APPLICATION(_, data) {
        localStorage.setItem('type', data)
    },
}